package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.identity.EndorsementResponseFilter;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.identity.IdentityPropertyDecoded;
import org.cweb.schemas.endorsements.EndorsementPayload;
import org.cweb.schemas.identity.IdentityDescriptor;

import java.util.ArrayList;
import java.util.List;

public class EndorsementProcessResponses extends Command {

    @Parameters(commandDescription = "Fetch and process endorsement response")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Sender Cweb ID")
        String idStr;

        @Parameter(names = "-hashToApply", description = "Hashcode of property list that should added to the own identity")
        Integer hashToApply;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public List<EndorsementProcessingResult> results = new ArrayList<>();

        public static class EndorsementProcessingResult {
            public int propertiesHash;
            public String action;
            public List<IdentityPropertyDecoded> properties;
        }
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();

        instanceContainer.getEndorsementService().checkAndProcessEndorsementResponsesFrom(identity, new EndorsementResponseFilter() {
            @Override
            public Action getAction(IdentityDescriptor identity, EndorsementPayload endorsementPayload) {
                Response.EndorsementProcessingResult resultForResponse = new Response.EndorsementProcessingResult();
                int hash = endorsementPayload.hashCode();
                boolean apply = request.hashToApply != null && request.hashToApply == hash;
                Action action = apply ? Action.APPLY : Action.IGNORE_ONCE;
                resultForResponse.propertiesHash = hash;
                resultForResponse.action = action.name();
                resultForResponse.properties = IdentityDescriptorDecoder.decode(endorsementPayload.getProperties());
                response.results.add(resultForResponse);
                return action;
            }
        });
        return response;
    }
}
