package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.InstanceContainerStateless;
import org.cweb.cli.Utils;
import org.cweb.identity.IdentityDescriptorDecoded;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.ThriftTextUtils;

public class IdentityReadStateless extends Command {

    @Parameters(commandDescription = "Fetch and print identity")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Cweb ID")
        public String idStr;

        @Parameter(names = "-publicStorageProfile", description = "Public storage profile")
        public String publicStorageProfileStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public IdentityDescriptorDecoded identityDescriptorDecoded;
    }

    @Override
    public boolean isStateless() {
        return true;
    }

    @Override
    public ResponseInterface runStateless(RequestInterface requestInterface, InstanceContainerStateless instanceContainerStateless) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        byte[] id = Utils.tryDecodingIdFromBase64or32(request.idStr);
        if (!IdentityService.isValidId(id)) {
            response.error = "Invalid id";
            return response;
        }

        if (request.publicStorageProfileStr == null) {
            response.error = "Missing publicStorageProfile";
            return response;
        }

        PublicStorageProfile publicStorageProfile = StorageProfileUtils.parsePublicStorageProfileHumanReadable(request.publicStorageProfileStr);
        if (publicStorageProfile == null) {
            publicStorageProfile = ThriftTextUtils.fromUrlSafeString(request.publicStorageProfileStr, PublicStorageProfile.class);
        }
        if (publicStorageProfile == null) {
            response.error = "Failed to parse storage profile";
            return response;
        }

        IdentityDescriptor identity = instanceContainerStateless.getRemoteIdentityService().fetchIdentityUncached(id, publicStorageProfile);
        if (identity == null) {
            response.error = "Failed to fetch identity descriptor";
            return response;
        }
        response.identityDescriptorDecoded = IdentityDescriptorDecoder.decode(identity);
        return response;
    }
}
