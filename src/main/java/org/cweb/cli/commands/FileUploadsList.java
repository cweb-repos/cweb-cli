package org.cweb.cli.commands;

import com.beust.jcommander.Parameters;

import org.cweb.InstanceContainer;
import org.cweb.files.FileUploadService;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.storage.NameConversionUtils;

import java.util.ArrayList;
import java.util.List;

public class FileUploadsList extends Command {

    @Parameters(commandDescription = "List uploaded files")
    public static class Request implements RequestInterface {
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public List<FileUploadState> fileUploadStates = new ArrayList<>();

        public static class FileUploadState {
            public String fileId;
            public TypedPayloadDecoded.FileMetadata fileMetadata;
            public FileUploadService.UploadState uploadState;
            public TypedPayloadDecoded.FileReference fileReference;
        }
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        List<byte[]> fileIds = instanceContainer.getFileUploadService().list();

        for (byte[] fileId : fileIds) {
            FileMetadata fileMeta = instanceContainer.getFileUploadService().getMetadata(fileId);
            FileUploadService.UploadState uploadState = instanceContainer.getFileUploadService().getUploadState(fileId);
            LocalUploadedFileInfo fileInfo = instanceContainer.getFileUploadService().getUploadedFileInfo(fileId);
            FileReference fileReference = fileInfo.getFileReference();

            Response.FileUploadState fileUploadState = new Response.FileUploadState();
            fileUploadState.fileId = NameConversionUtils.toString(fileId);
            fileUploadState.fileReference = TypedPayloadDecoder.decodeFileReference(fileReference);
            fileUploadState.fileMetadata = TypedPayloadDecoder.decodeFileMetadata(fileMeta);
            fileUploadState.uploadState = uploadState;

            response.fileUploadStates.add(fileUploadState);
        }
        return response;
    }
}
