package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Triple;
import org.cweb.InstanceContainer;
import org.cweb.files.FileUploadService;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.files.FileReference;
import org.cweb.schemas.files.LocalUploadedFileInfo;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;

import java.io.File;
import java.util.Collections;

public class FileUpload extends Command {

    @Parameters(commandDescription = "Upload file")
    public static class Request implements RequestInterface {
        @Parameter(names = "-filePath", description = "File path to upload")
        public String filePath;

        @Parameter(names = "-remoteFileName", description = "Remote file name. If not provided, defaults to the local filename")
        public String remoteFileName;

        @Parameter(names = "-filePartSizeBytes", description = "Size in bytes of a file part")
        public Integer filePartSizeBytes = 500000;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public String fileId;
        public String key;
        public TypedPayloadDecoded.FileMetadata fileMetadata;
        public FileUploadService.UploadState uploadState;
        public long tookTime;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();
        long startTime = System.currentTimeMillis();

        String externalName = request.remoteFileName != null ? request.remoteFileName : (new File(request.filePath)).getName();
        Triple<LocalUploadedFileInfo, FileMetadata, FileUploadService.UploadError> result = instanceContainer.getFileUploadService().upload(request.filePath, externalName, Collections.emptyList(), request.filePartSizeBytes, false);

        if (result.getRight() != null) {
            response.error = "Error uploading file: " + result.getRight();
            return response;
        }

        LocalUploadedFileInfo uploadedFileInfo = result.getLeft();
        FileReference fileReference = uploadedFileInfo.getFileReference();
        byte[] fileId = fileReference.getFileId();

        response.fileId = NameConversionUtils.toString(fileId);
        response.key = NameConversionUtils.toString(fileReference.getKey());
        response.fileMetadata = TypedPayloadDecoder.decodeFileMetadata(result.getMiddle());

        FileUploadService.UploadState uploadState;
        int uploadedPercentsPrev = 0;
        while (true) {
            uploadState = instanceContainer.getFileUploadService().getUploadState(fileId);
            if (uploadState == null || uploadState.corrupted || uploadState.uploadedFraction == 1.0) {
                break;
            }
            int uploadedPercents = (int) (uploadState.uploadedFraction * 100);
            if (uploadedPercents != uploadedPercentsPrev) {
                System.out.println("Completed " + uploadedPercents + "%");
                uploadedPercentsPrev = uploadedPercents;
            }
            Threads.sleepChecked(1000);
        }

        long endTime = System.currentTimeMillis();
        response.uploadState = uploadState;
        response.tookTime = endTime - startTime;
        return response;
    }
}
