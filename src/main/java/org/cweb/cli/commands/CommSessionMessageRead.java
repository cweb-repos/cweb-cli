package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.communication.CommSessionService;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.wire.TypedPayload;

import java.util.ArrayList;
import java.util.List;

public class CommSessionMessageRead extends Command {

    @Parameters(commandDescription = "Receive message from communication session")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Sender Cweb ID")
        public String idStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public List<TypedPayloadDecoded> messages = new ArrayList<>();
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();

        List<CommSessionService.ReceivedMessage> messages = instanceContainer.getCommScheduler().immediateCommSessionRead(identity.getId());

        for (CommSessionService.ReceivedMessage message : messages) {
            response.messages.add(TypedPayloadDecoder.decodePayload(message.payload));
        }
        return response;
    }
}
