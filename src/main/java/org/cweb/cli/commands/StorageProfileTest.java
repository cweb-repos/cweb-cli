package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.AccountUtils;
import org.cweb.InstanceContainer;
import org.cweb.InstanceContainerStateless;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;

public class StorageProfileTest extends Command {

    @Parameters(commandDescription = "Test whether a private storage is functional")
    public static class Request implements RequestInterface {
        @Parameter(names = "-privateStorageProfile", description = "Private storage profile")
        public String privateStorageProfileStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public boolean success;
    }

    @Override
    public boolean isStateless() {
        return true;
    }

    @Override
    public ResponseInterface runStateless(RequestInterface requestInterface, InstanceContainerStateless instanceContainerStateless) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        PrivateStorageProfile privateStorageProfile = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(request.privateStorageProfileStr);
        if (privateStorageProfile == null) {
            response.error = "Failed to parse storage profile";
            return response;
        }
        response.success = AccountUtils.testStorageProfile(privateStorageProfile);
        return response;
    }
}
