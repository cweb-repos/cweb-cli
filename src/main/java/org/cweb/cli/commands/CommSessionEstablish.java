package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.wire.TypedPayload;

public class CommSessionEstablish extends Command {

    @Parameters(commandDescription = "Establish new communication session")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Counterpart Cweb ID")
        public String idStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();

        boolean success = instanceContainer.getCommSessionService().establishSessionWith(identity.getId());
        response.success = success;
        return response;
    }
}
