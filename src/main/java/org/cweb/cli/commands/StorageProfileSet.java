package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.identity.IdentityDescriptorDecoded;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.ThriftTextUtils;

public class StorageProfileSet extends Command {

    @Parameters(commandDescription = "Set storage profile for an identity")
    public static class Request implements RequestInterface {
        @Parameter(names = "-publicStorageProfile", description = "Public storage profile")
        public String publicStorageProfileStr;

        @Parameter(names = "-id", description = "Cweb ID")
        public String idStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public IdentityDescriptorDecoded identityDescriptorDecoded;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        byte[] id = org.cweb.cli.Utils.tryDecodingIdFromBase64or32(request.idStr);
        if (!IdentityService.isValidId(id)) {
            response.error = "Invalid id";
            return response;
        }

        PublicStorageProfile publicStorageProfile = StorageProfileUtils.parsePublicStorageProfileHumanReadable(request.publicStorageProfileStr);
        if (publicStorageProfile == null) {
            publicStorageProfile = ThriftTextUtils.fromUrlSafeString(request.publicStorageProfileStr, PublicStorageProfile.class);
        }
        if (publicStorageProfile == null) {
            response.error = "Failed to parse storage profile";
            return response;
        }
        boolean publicStorageProfileSuccess = instanceContainer.getRemoteIdentityService().setRemoteStorageProfile(id, publicStorageProfile);
        if (!publicStorageProfileSuccess) {
            response.error = "Failed to confirm storage profile";
            return response;
        }

        LocalIdentityDescriptorState descriptorState = instanceContainer.getRemoteIdentityService().get(id);
        if (descriptorState == null) {
            response.error = "Failed to fetch identity descriptor";
            return response;
        }
        response.identityDescriptorDecoded = IdentityDescriptorDecoder.decode(descriptorState);
        return response;
    }
}
