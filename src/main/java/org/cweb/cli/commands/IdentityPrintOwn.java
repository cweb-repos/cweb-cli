package org.cweb.cli.commands;

import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.identity.IdentityDescriptorDecoded;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.ThriftTextUtils;

public class IdentityPrintOwn extends Command {

    @Parameters(commandDescription = "Print own identity descriptor")
    public static class Request implements RequestInterface {
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public IdentityDescriptorDecoded identityDescriptorDecoded;
        public String idUrlSafe;
        public String publicStorageProfileUrlSafe;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Response response = new Response();

        IdentityDescriptor identity = instanceContainer.getIdentityService().getIdentityDescriptor();
        if (identity == null) {
            return response;
        }

        response.idUrlSafe = org.cweb.utils.Utils.toBase32String(identity.getId());
        response.publicStorageProfileUrlSafe = ThriftTextUtils.toUrlSafeString(identity.getStorageProfile());
        response.identityDescriptorDecoded = IdentityDescriptorDecoder.decode(identity);

        return response;
    }
}
