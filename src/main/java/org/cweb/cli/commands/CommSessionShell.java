package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.communication.CommSessionMessageCallback;
import org.cweb.communication.CommSessionService;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.wire.TypedPayload;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

public class CommSessionShell extends Command {

    @Parameters(commandDescription = "Interactive comm sessions shell")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Recipient Cweb ID")
        public String idStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, final InstanceContainer instanceContainer) {
        CommSessionShell.Request request = (CommSessionShell.Request) requestInterface;
        Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();
        final byte[] id = identity.getId();
        SessionId sessionIdObj = new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(id));

        final LineReader reader;
        try {
            Terminal terminal = TerminalBuilder.terminal();
            reader = LineReaderBuilder.builder().terminal(terminal).option(LineReader.Option.ERASE_LINE_ON_FINISH, true).build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        instanceContainer.getCommScheduler().setCommSessionMessageCallback(new CommSessionMessageCallback() {
            @Override
            public void onMessagesReceived(byte[] fromId) {
                if (Arrays.equals(id, fromId)) {
                    printMessages(instanceContainer, id, reader);
                    instanceContainer.getCommScheduler().reportInteraction(sessionIdObj);
                }
            }
        });

        printMessages(instanceContainer, id, reader);
        instanceContainer.getCommScheduler().requestPeriodicSync(new SessionId(SessionType.COMM_SESSION, ByteBuffer.wrap(id)), 1000L * 3, 1000L * 60 * 5);

        while (true) {
            String line = reader.readLine("me > ");

            if (":q".equals(line) || "exit".equals(line)) {
                break;
            }

            instanceContainer.getCommScheduler().reportInteraction(sessionIdObj);

            if (!StringUtils.isBlank(line)) {
                reader.printAbove(Utils.getMessageShellPrefix(instanceContainer, instanceContainer.getOwnId(), true, System.currentTimeMillis()) + line);
                TypedPayload message = TypedPayloadUtils.wrapCustom(line.getBytes(), "CLI", "Message", null);
                boolean success = instanceContainer.getCommSessionService().sendMessage(id, message);
                if (!success) {
                    System.out.println("Failed to send: " + line);
                }
            }
        }

        return response;
    }

    private void printMessages(InstanceContainer instanceContainer, byte[] id, LineReader reader) {
        List<CommSessionService.ReceivedMessage> messages = instanceContainer.getCommSessionService().fetchNewMessages(id, true);
        if (messages != null) {
            for (CommSessionService.ReceivedMessage message : messages) {
                reader.printAbove(Utils.getMessageShellPrefix(instanceContainer, id, false, message.createdAt) + new String(message.payload.getData()));
            }
        }
    }
}
