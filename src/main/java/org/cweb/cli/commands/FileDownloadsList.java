package org.cweb.cli.commands;

import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.files.FileDownloadService;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.storage.NameConversionUtils;

import java.util.ArrayList;
import java.util.List;

public class FileDownloadsList extends Command {

    @Parameters(commandDescription = "List downloaded files")
    public static class Request implements RequestInterface {
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public List<FileDownloadState> fileDownloadStates = new ArrayList<>();

        public static class FileDownloadState {
            public String fromId;
            public String fileId;
            public TypedPayloadDecoded.FileMetadata fileMetadata;
            public FileDownloadService.DownloadState downloadState;
        }
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        List<Pair<byte[], byte[]>> downloads = instanceContainer.getFileDownloadService().listAllDownloads();

        for (Pair<byte[], byte[]> fromIdAndfileId : downloads) {
            Response.FileDownloadState fileDownloadState = new Response.FileDownloadState();
            byte[] fromId = fromIdAndfileId.getLeft();
            byte[] fileId = fromIdAndfileId.getRight();
            FileMetadata fileMeta = instanceContainer.getFileDownloadService().getMetadataOfCurrentDownload(fromId, fileId);
            FileDownloadService.DownloadState downloadState = instanceContainer.getFileDownloadService().getDownloadState(fromId, fileId);

            fileDownloadState.fromId = NameConversionUtils.toString(fromId);
            fileDownloadState.fileId = NameConversionUtils.toString(fileId);
            fileDownloadState.fileMetadata = TypedPayloadDecoder.decodeFileMetadata(fileMeta);
            fileDownloadState.downloadState = downloadState;
            response.fileDownloadStates.add(fileDownloadState);
        }
        return response;
    }
}
