package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.cli.InstanceFactory;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.schemas.storage.PublicStorageProfile;
import org.cweb.storage.NameConversionUtils;
import org.cweb.storage.remote.StorageProfileUtils;
import org.cweb.utils.Utils;

public class StorageProfileSetOwn extends Command {

    @Parameters(commandDescription = "Update own storage profile")
    public static class Request implements RequestInterface {
        @Parameter(names = "-privateStorageProfile", description = "Private storage profile")
        public String privateStorageProfileStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        PrivateStorageProfile privateStorageProfile = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(request.privateStorageProfileStr);
        if (privateStorageProfile == null) {
            response.error = "Failed to parse storage profile";
            return response;
        }
        Pair<Boolean, String> publicStorageProfileSuccess = instanceContainer.updatePrivateStorageProfile(privateStorageProfile);
        if (publicStorageProfileSuccess.getRight() != null) {
            response.error = "Failed to confirm storage profile: " + publicStorageProfileSuccess.getRight();
        }

        response.success = true;

        return response;
    }
}
