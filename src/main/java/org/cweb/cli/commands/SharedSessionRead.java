package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.communication.SharedSessionService;
import org.cweb.identity.IdentityService;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.storage.NameConversionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SharedSessionRead extends Command {

    @Parameters(commandDescription = "Receive shared sessions messages")
    public static class Request implements RequestInterface {
        @Parameter(names = "-sessionId", description = "Session id")
        public String sessionId;

        @Parameter(names = "-consume", description = "Consume messages")
        public boolean consume;

        @Parameter(names = "-seedPeerId", description = "Cweb Id of a peer from whom to bootstrap session state")
        public String seedPeerId;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public List<Message> messages;

        public static class Message {
            public String fromId;
            public String createdAt;
            public TypedPayloadDecoded payload;
        }
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        SharedSessionRead.Request request = (SharedSessionRead.Request) requestInterface;
        Response response = new Response();

        byte[] sessionId = NameConversionUtils.fromString(request.sessionId);
        SharedSessionService.SessionMetadata metadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId);
        List<byte[]> participants = Utils.getParticipants(instanceContainer, request.seedPeerId, metadata);

        instanceContainer.getCommScheduler().immediateCommSessionRead(participants);
        instanceContainer.getCommScheduler().flushSharedSessionQueue();

        List<SharedSessionService.ReceivedMessage> messages = instanceContainer.getSharedSessionService().fetchNewMessages(sessionId, request.consume);
        if (messages == null) {
            response.error = "Unknown session";
            return response;
        }

        List<Response.Message> messagesTransformed = new ArrayList<>();
        for (SharedSessionService.ReceivedMessage message : messages) {
            Response.Message messageTransformed = new Response.Message();
            messageTransformed.fromId = IdentityService.toString(message.fromId);
            messageTransformed.createdAt = org.cweb.utils.Utils.simpleDateFormat.format(new Date(message.createdAt));
            messageTransformed.payload = TypedPayloadDecoder.decodePayload(message.payload);
            messagesTransformed.add(messageTransformed);
        }
        response.messages = messagesTransformed;
        return response;
    }
}
