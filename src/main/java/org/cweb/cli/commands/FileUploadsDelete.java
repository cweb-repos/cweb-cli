package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.Utils;

public class FileUploadsDelete extends Command {

    @Parameters(commandDescription = "Delete previously uploaded file")
    public static class Request implements RequestInterface {
        @Parameter(names = "-fileId", description = "File ID to download")
        public String fileIdStr;

        @Parameter(names = "-force", description = "Force deletion even when there are partial failures")
        public Boolean force;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String fileId;
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        byte[] fileId = NameConversionUtils.fromString(request.fileIdStr);
        boolean success = instanceContainer.getFileUploadService().delete(fileId, request.force != null ? request.force : false);

        response.fileId = NameConversionUtils.toString(fileId);
        response.success = success;
        return response;
    }
}
