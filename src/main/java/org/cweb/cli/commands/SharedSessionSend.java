package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.NameConversionUtils;

import java.util.Collections;

public class SharedSessionSend extends Command {

    @Parameters(commandDescription = "Send a shared sessions message")
    public static class Request implements RequestInterface {
        @Parameter(names = "-sessionId", description = "Session id")
        public String sessionId;

        @Parameter(names = "-message", description = "Message text")
        public String message;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        SharedSessionSend.Request request = (SharedSessionSend.Request) requestInterface;
        Response response = new Response();

        byte[] sessionId = NameConversionUtils.fromString(request.sessionId);
        TypedPayload message = TypedPayloadUtils.wrapCustom(request.message.getBytes(), "CLI", "Message", null);
        response.success = instanceContainer.getSharedSessionService().sendMessage(sessionId, message);
        return response;
    }
}
