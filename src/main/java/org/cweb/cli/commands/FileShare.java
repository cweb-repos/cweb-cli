package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.communication.SharedSessionService;
import org.cweb.files.FileUploadService;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.storage.NameConversionUtils;

import java.nio.ByteBuffer;

public class FileShare extends Command {

    @Parameters(commandDescription = "Share file with another user")
    public static class Request implements RequestInterface {
        @Parameter(names = "-fileId", description = "File id to share")
        public String fileIdStr;

        @Parameter(names = "-shareDirectlyToId", description = "Cweb ID to share to")
        public String toIdStr;

        @Parameter(names = "-shareToSharedSessionId", description = "Shared session ID to share to")
        public String sessionIdStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        byte[] fileId = NameConversionUtils.fromString(request.fileIdStr);
        if (fileId == null || !FileUploadService.isValidFileId(fileId)) {
            response.error = "Invalid fileId";
            return response;
        }

        SessionId sessionId;

        if (request.toIdStr != null) {
            Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.toIdStr);
            if (identityResult.getRight() != null) {
                response.error = identityResult.getRight();
                return response;
            }
            byte[] toId = identityResult.getLeft().getId();
            sessionId = new SessionId(SessionType.COMM_SESSION, ByteBuffer.wrap(toId));
        } else if (request.sessionIdStr != null) {
            byte[] sharedSessionId = NameConversionUtils.fromString(request.sessionIdStr);
            if (sharedSessionId == null || !SharedSessionService.isValidSessionId(sharedSessionId)) {
                response.error = "Invalid fileId";
                return response;
            }
            sessionId = new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sharedSessionId));
        } else {
            response.error = "Missing sharing destination";
            return response;
        }

        response.success = instanceContainer.getFileUploadService().share(fileId, sessionId);

        return response;
    }
}
