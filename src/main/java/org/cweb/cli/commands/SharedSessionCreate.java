package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.PropertyUtils;

import java.util.Collections;

public class SharedSessionCreate extends Command {

    @Parameters(commandDescription = "Create new shared session")
    public static class Request implements RequestInterface {
        @Parameter(names = "-name", description = "Session name")
        public String name;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public String sessionId;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        SharedSessionCreate.Request request = (SharedSessionCreate.Request) requestInterface;
        Response response = new Response();

        Property nameProperty = new Property(PropertyUtils.PROPERTY_KEY_NICK_NAME, PropertyValue.str(request.name));
        byte[] sessionId = instanceContainer.getSharedSessionService().createNewSession(Collections.singletonList(nameProperty));
        response.sessionId = NameConversionUtils.toString(sessionId);
        return response;
    }
}
