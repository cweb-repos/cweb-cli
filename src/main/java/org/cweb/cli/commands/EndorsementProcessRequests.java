package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.identity.EndorsementVerifier;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.identity.IdentityPropertyDecoded;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.properties.Property;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EndorsementProcessRequests extends Command {

    @Parameters(commandDescription = "Fetch and process endorsement request")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Sender Cweb ID")
        String idStr;

        @Parameter(names = "-hashToEndorse", description = "Hashcode of property list that should be endorsed")
        Integer hashToEndorse;

        @Parameter(names = "-hashToFail", description = "Hashcode of property list that should be failed")
        Integer hashToFail;

        @Parameter(names = "-failureMessage", description = "Message to return with failure response")
        String failureMessage;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public List<EndorsementProcessingResult> results = new ArrayList<>();

        public static class EndorsementProcessingResult {
            public int propertiesHash;
            public String result;
            public List<IdentityPropertyDecoded> properties;
        }
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        Preconditions.checkArgument((request.hashToEndorse == null && request.hashToFail == null) || !Objects.equals(request.hashToEndorse, request.hashToFail));

        Pair<IdentityDescriptor, String> identityResult = Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();

        instanceContainer.getEndorsementService().checkAndProcessEndorsementRequestsFrom(identity, new EndorsementVerifier() {
            @Override
            public Pair<Result, String> verify(IdentityDescriptor identity, List<Property> properties) {
                Response.EndorsementProcessingResult resultForResponse = new Response.EndorsementProcessingResult();
                int hash = properties.hashCode();
                boolean endorse = request.hashToEndorse != null && request.hashToEndorse == hash;
                boolean fail = request.hashToFail != null && request.hashToFail == hash;
                Preconditions.checkArgument(!(endorse && fail));
                Pair<Result, String> result;
                if (endorse) {
                    result = Pair.of(Result.SUCCESS, null);
                } else if (fail) {
                    result = Pair.of(Result.FAIL, request.failureMessage);
                } else {
                    result = Pair.of(Result.IGNORE_ONCE, null);
                }
                resultForResponse.propertiesHash = hash;
                resultForResponse.result = result.getLeft().name();
                resultForResponse.properties = IdentityDescriptorDecoder.decode(properties);
                response.results.add(resultForResponse);
                return result;
            }
        });
        return response;
    }
}
