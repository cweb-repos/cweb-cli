package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.communication.SharedSessionService;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.properties.Property;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.PropertyUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SharedSessionList extends Command {

    @Parameters(commandDescription = "List all known shared sessions")
    public static class Request implements RequestInterface {
        @Parameter(names = "-sessionId", description = "Session id")
        public String sessionId;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public List<SessionMetadata> sessions = new ArrayList<>();

        public static class SessionMetadata {
            public String sessionId;
            public String adminId;
            public String name;
            public List<String> participants;
        }
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        SharedSessionList.Request request = (SharedSessionList.Request) requestInterface;
        Response response = new Response();

        byte[] sessionIdParam = request.sessionId != null ? NameConversionUtils.fromString(request.sessionId) : null;
        List<byte[]> sessionIds = instanceContainer.getSharedSessionService().getActiveSessionIds();

        for (byte[] sessionId : sessionIds) {
            if (sessionIdParam != null && !Arrays.equals(sessionIdParam, sessionId)) {
                continue;
            }
            SharedSessionService.SessionMetadata metadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId);
            Response.SessionMetadata session = new Response.SessionMetadata();
            session.sessionId = NameConversionUtils.toString(sessionId);
            session.adminId = NameConversionUtils.toString(metadata.adminId);
            Property nameProperty = PropertyUtils.findProperty(metadata.properties, PropertyUtils.PROPERTY_KEY_NICK_NAME);
            session.name = nameProperty != null ? nameProperty.getValue().getStr() : null;
            List<String> participants = new ArrayList<>();
            for (byte[] participantId : metadata.participantIds) {
                participants.add(IdentityService.toString(participantId));
            }
            session.participants = participants;
            response.sessions.add(session);
        }

        return response;
    }
}
