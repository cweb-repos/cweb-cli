package org.cweb.cli.commands;

import org.cweb.cli.InstanceFactory;

public abstract class AdminCommand extends Command {
    public abstract ResponseInterface run(RequestInterface requestInterface, InstanceFactory instanceFactory) throws Exception;
}
