package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.storage.NameConversionUtils;

import java.util.ArrayList;
import java.util.List;

public class SharedSessionUpdateParticipants extends Command {

    @Parameters(commandDescription = "Update participants shared sessions")
    public static class Request implements RequestInterface {
        @Parameter(names = "-sessionId", description = "Session id")
        public String sessionId;

        @Parameter(names = "-ids", description = "Participant ids to add/remove")
        public List<String> ids;

        @Parameter(names = "-remove", description = "Remove participants")
        public boolean remove;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        SharedSessionUpdateParticipants.Request request = (SharedSessionUpdateParticipants.Request) requestInterface;
        Response response = new Response();

        byte[] sessionId = NameConversionUtils.fromString(request.sessionId);
        List<byte[]> ids = new ArrayList<>();
        for (String idStr : request.ids) {
            byte[] id = Utils.tryDecodingIdFromBase64or32(idStr);
            if (id == null) {
                response.error = "Invalid id " + idStr;
                return response;
            }
            ids.add(id);
        }

        boolean add = !request.remove;
        response.success = instanceContainer.getSharedSessionService().updateParticipants(sessionId, add, ids);
        return response;
    }
}
