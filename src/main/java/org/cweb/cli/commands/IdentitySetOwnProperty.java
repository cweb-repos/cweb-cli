package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import org.cweb.InstanceContainer;
import org.cweb.identity.IdentityDescriptorDecoded;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;
import org.cweb.utils.PropertyUtils;

import java.util.ArrayList;
import java.util.List;

public class IdentitySetOwnProperty extends Command {

    @Parameters(commandDescription = "Set own property")
    public static class Request implements RequestInterface {
        @Parameter(names = "-nickName", description = "Nick name")
        String nickName;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public IdentityDescriptorDecoded identityDescriptorDecoded;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        List<Property> properties = new ArrayList<>();
        if (request.nickName != null) {
            Property property = new Property(PropertyUtils.PROPERTY_KEY_NICK_NAME, PropertyValue.str(request.nickName));
            properties.add(property);
        }

        instanceContainer.getIdentityService().updateProperties(properties);
        IdentityDescriptor identity = instanceContainer.getIdentityService().getIdentityDescriptor();
        if (identity == null) {
            return response;
        }

        response.identityDescriptorDecoded = IdentityDescriptorDecoder.decode(identity);

        return response;
    }
}
