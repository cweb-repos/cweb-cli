package org.cweb.cli.commands;

import org.apache.commons.lang3.NotImplementedException;
import org.cweb.InstanceContainer;
import org.cweb.InstanceContainerStateless;

public abstract class Command {
    public abstract RequestInterface getRequest();
    public boolean isStateless() {
        return false;
    }
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) throws Exception {
        throw new NotImplementedException();
    }
    public ResponseInterface runStateless(RequestInterface requestInterface, InstanceContainerStateless instanceContainerStateless) throws Exception {
        throw new NotImplementedException();
    }
}
