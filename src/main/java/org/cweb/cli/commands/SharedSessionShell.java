package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import org.apache.commons.lang3.StringUtils;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.communication.SharedSessionCallback;
import org.cweb.communication.SharedSessionService;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.comm.SessionId;
import org.cweb.schemas.comm.SessionType;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.storage.NameConversionUtils;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

public class SharedSessionShell extends Command {

    public static final long MIN_SYNC_INTERVAL = 1000L * 3;
    public static final long MAX_SYNC_INTERVAL = 1000L * 60 * 5;

    @Parameters(commandDescription = "Interactive shared sessions shell")
    public static class Request implements RequestInterface {
        @Parameter(names = "-sessionId", description = "Session id")
        public String sessionId;

        @Parameter(names = "-seedPeerId", description = "Cweb Id of a peer from whom to bootstrap session state")
        public String seedPeerId;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, final InstanceContainer instanceContainer) {
        SharedSessionShell.Request request = (SharedSessionShell.Request) requestInterface;
        Response response = new Response();

        final byte[] sessionId = NameConversionUtils.fromString(request.sessionId);
        if (sessionId == null) {
            response.error = "Missing or invalid sessionId";
            return response;
        }
        SessionId sessionIdObj = new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId));

        if (request.seedPeerId != null) {
            final byte[] seedPeerId = NameConversionUtils.fromString(request.seedPeerId);
            if (seedPeerId == null) {
                response.error = "Missing or invalid seedPeerId";
                return response;
            }
            instanceContainer.getCommScheduler().immediateCommSessionRead(seedPeerId);
            instanceContainer.getCommScheduler().flushSharedSessionQueue();
        }

        final LineReader reader;
        try {
            Terminal terminal = TerminalBuilder.terminal();
            reader = LineReaderBuilder.builder().terminal(terminal).option(LineReader.Option.ERASE_LINE_ON_FINISH, true).build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        instanceContainer.getCommScheduler().setSharedSessionCallback(new SharedSessionCallback() {
            @Override
            public void onMessagesReceived(byte[] receivedSessionId) {
                if (Arrays.equals(sessionId, receivedSessionId)) {
                    printMessages(instanceContainer, sessionId, reader);
                    instanceContainer.getCommScheduler().reportInteraction(sessionIdObj);
                }
            }

            @Override
            public void onMessagesAcked(byte[] sessionId) {
            }

            @Override
            public void onDescriptorUpdated(byte[] sessionIdUpdated) {
                if (!Arrays.equals(sessionId, sessionIdUpdated)) {
                    return;
                }
                instanceContainer.getCommScheduler().requestPeriodicSync(new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId)), MIN_SYNC_INTERVAL, MAX_SYNC_INTERVAL);
            }
        });

        instanceContainer.getCommScheduler().requestPeriodicSync(new SessionId(SessionType.SHARED_SESSION, ByteBuffer.wrap(sessionId)), MIN_SYNC_INTERVAL, MAX_SYNC_INTERVAL);
        instanceContainer.getCommScheduler().flushSharedSessionQueue();

        SharedSessionService.SessionMetadata metadata = instanceContainer.getSharedSessionService().getSessionMetadata(sessionId);
        if (metadata == null) {
            response.error = "Unknown session";
            return response;
        }

        printMessages(instanceContainer, sessionId, reader);

        while (true) {
            String line = reader.readLine("me > ");

            if (":q".equals(line) || "exit".equals(line)) {
                break;
            }

            instanceContainer.getCommScheduler().reportInteraction(sessionIdObj);

            if (!StringUtils.isBlank(line)) {
                reader.printAbove(Utils.getMessageShellPrefix(instanceContainer, instanceContainer.getOwnId(), true, System.currentTimeMillis()) + line);
                TypedPayload message = TypedPayloadUtils.wrapCustom(line.getBytes(), "CLI", "Message", null);
                boolean success = instanceContainer.getSharedSessionService().sendMessage(sessionId, message);
                if (!success) {
                    System.out.println("Failed to send: " + line);
                }
            }
        }

        return response;
    }

    private synchronized void printMessages(InstanceContainer instanceContainer, byte[] sessionId, LineReader reader) {
        List<SharedSessionService.ReceivedMessage> messages = instanceContainer.getSharedSessionService().fetchNewMessages(sessionId, true);
        if (messages != null) {
            for (SharedSessionService.ReceivedMessage message : messages) {
                reader.printAbove(Utils.getMessageShellPrefix(instanceContainer, message.fromId, false, message.createdAt) + new String(message.payload.getData()));
            }
        }
    }
}
