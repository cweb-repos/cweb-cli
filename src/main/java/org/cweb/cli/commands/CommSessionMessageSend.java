package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.payload.TypedPayloadUtils;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.utils.Utils;

public class CommSessionMessageSend extends Command {

    @Parameters(commandDescription = "Send message via communication session")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Recipient Cweb ID")
        public String idStr;

        @Parameter(names = "-message", description = "Message text")
        public String message;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public TypedPayloadDecoded message;
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();

        TypedPayload message = TypedPayloadUtils.wrapCustom(request.message.getBytes(), "CLI", "Message", null);
        boolean success = instanceContainer.getCommSessionService().sendMessage(identity.getId(), message);
        response.message = TypedPayloadDecoder.decodePayload(message);
        response.success = success;
        return response;
    }
}
