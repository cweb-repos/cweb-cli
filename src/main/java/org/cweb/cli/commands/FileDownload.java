package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.files.FileDownloadService;
import org.cweb.files.FileUploadService;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.schemas.files.FileMetadata;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.Threads;
import org.cweb.utils.Utils;

import java.io.File;
import java.nio.file.Paths;

public class FileDownload extends Command {

    @Parameters(commandDescription = "Download File")
    public static class Request implements RequestInterface {
        @Parameter(names = "-fromId", description = "Originator Cweb ID")
        public String fromIdStr;

        @Parameter(names = "-fileId", description = "File ID to download")
        public String fileIdStr;

        @Parameter(names = "-localPath", description = "Local path. If the path is a directory then will use remote filename.")
        public String localPath;

        @Parameter(names = "-metadataOnly", description = "Only download and print metadata.")
        public boolean metadataOnly;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public TypedPayloadDecoded.FileMetadata fileMetadata;
        public boolean success;
        public long tookTime;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.fromIdStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        byte[] fromId = identityResult.getLeft().getId();
        byte[] fileId = NameConversionUtils.fromString(request.fileIdStr);
        if (fileId == null || !FileUploadService.isValidFileId(fileId)) {
            response.error = "Invalid fileId";
            return response;
        }

        Pair<FileMetadata, FileDownloadService.DownloadError> fileMetaResult = instanceContainer.getFileDownloadService().readFileMeta(fromId, fileId);

        if (fileMetaResult.getRight() != null) {
            response.error = "Error downloading metadata: " + fileMetaResult.getRight();
            return response;
        }

        FileMetadata fileMetadata = fileMetaResult.getLeft();
        response.fileMetadata = TypedPayloadDecoder.decodeFileMetadata(fileMetadata);

        if (request.metadataOnly) {
            return response;
        }

        if (request.localPath == null) {
            response.error = "Missing localPath";
            return response;
        }

        boolean isLocalPathDirectory = (new File(request.localPath)).isDirectory();
        if (isLocalPathDirectory && !fileMetadata.isSetName()) {
            response.error = "Filename missing";
            return response;
        }

        String localName = isLocalPathDirectory ? Paths.get(request.localPath, fileMetadata.getName()).toString() : request.localPath;

        Pair<FileMetadata, FileDownloadService.DownloadError> downloadResult = instanceContainer.getFileDownloadService().startDownload(fromId, fileId, localName);
        if (downloadResult.getRight() != null) {
            response.error = "Error starting download: " + downloadResult.getRight();
            return response;
        }

        long startTime = System.currentTimeMillis();
        FileDownloadService.DownloadState downloadState;
        int downloadedPercentsPrev = 0;
        while (true) {
            downloadState = instanceContainer.getFileDownloadService().getDownloadState(fromId, fileId);
            if (downloadState.completedAt != null || downloadState.abortedAt != null) {
                break;
            }
            int downloadedPercents = (int) (downloadState.downloadedFraction * 100);
            if (downloadedPercents != downloadedPercentsPrev) {
                System.out.println("Completed " + downloadedPercents + "%");
                downloadedPercentsPrev = downloadedPercents;
            }
            Threads.sleepChecked(100);
        }
        long endTime = System.currentTimeMillis();
        response.success = downloadState.completedAt != null;
        response.tookTime = endTime - startTime;
        return response;
    }
}
