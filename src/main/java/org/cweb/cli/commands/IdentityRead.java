package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.identity.IdentityDescriptorDecoded;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.identity.LocalIdentityDescriptorState;

public class IdentityRead extends Command {

    @Parameters(commandDescription = "Print identity descriptor")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Cweb ID")
        public String idStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public IdentityDescriptorDecoded identityDescriptorDecoded;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        byte[] id = Utils.tryDecodingIdFromBase64or32(request.idStr);
        if (!IdentityService.isValidId(id)) {
            response.error = "Invalid id";
            return response;
        }

        LocalIdentityDescriptorState descriptorState = instanceContainer.getRemoteIdentityService().get(id);
        if (descriptorState == null) {
            response.error = "Failed to fetch identity descriptor";
            return response;
        }
        response.identityDescriptorDecoded = IdentityDescriptorDecoder.decode(descriptorState);
        return response;
    }
}
