package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.cweb.InstanceContainer;
import org.cweb.cli.InstanceFactory;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.identity.IdentityReference;
import org.cweb.schemas.storage.PrivateStorageProfile;
import org.cweb.storage.remote.StorageProfileUtils;

public class IdentityCreateNew extends AdminCommand {

    @Parameters(commandDescription = "Create new identity")
    public static class Request implements RequestInterface {
        @Parameter(names = "-privateStorageProfile", description = "Private storage profile")
        public String privateStorageProfileStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public String idStr;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) throws Exception {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceFactory instanceFactory) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        PrivateStorageProfile privateStorageProfile = StorageProfileUtils.parsePrivateStorageProfileHumanReadable(request.privateStorageProfileStr);
        if (privateStorageProfile == null) {
            response.error = "Failed to parse storage profile";
            return response;
        }
        InstanceContainer instanceContainer = instanceFactory.createNewIdentity(privateStorageProfile);
        if (instanceContainer == null) {
            response.error = "Failed to create id";
            return response;
        }
        response.idStr = IdentityService.toString(instanceContainer.getOwnId());

        return response;
    }
}
