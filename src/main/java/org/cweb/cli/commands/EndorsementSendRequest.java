package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.identity.IdentityDescriptorDecoder;
import org.cweb.identity.IdentityPropertyDecoded;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.properties.Property;
import org.cweb.schemas.properties.PropertyValue;

import java.util.ArrayList;
import java.util.List;

public class EndorsementSendRequest extends Command {

    @Parameters(commandDescription = "Construct and send endorsement request")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Recipient Cweb ID")
        public String idStr;

        @Parameter(names = "-key", description = "Endorsement key")
        public String key;

        @Parameter(names = "-value", description = "Endorsement value")
        public String value;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public List<IdentityPropertyDecoded> properties;
        public boolean success;
    }

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        final Request request = (Request) requestInterface;
        final Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();

        List<Property> properties = new ArrayList<>();
        Property property = new Property(request.key, PropertyValue.str(request.value));
        properties.add(property);
        boolean success = instanceContainer.getEndorsementService().publishEndorsementRequestFor(identity, properties);
        response.properties = IdentityDescriptorDecoder.decode(properties);
        response.success = success;
        return response;
    }
}
