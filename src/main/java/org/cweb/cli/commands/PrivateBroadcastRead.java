package org.cweb.cli.commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.cli.Utils;
import org.cweb.payload.PayloadTypePredicate;
import org.cweb.payload.TypedPayloadDecoded;
import org.cweb.payload.TypedPayloadDecoder;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.wire.TypedPayload;
import org.cweb.schemas.wire.TypedPayloadMetadata;

import java.util.ArrayList;
import java.util.List;

public class PrivateBroadcastRead extends Command {

    @Parameters(commandDescription = "Receive private broadcast")
    public static class Request implements RequestInterface {
        @Parameter(names = "-id", description = "Sender Cweb ID")
        String idStr;
    }

    public Request getRequest() {
        return new Request();
    }

    public static class Response implements ResponseInterface {
        public String error;
        public List<TypedPayloadDecoded> messages = new ArrayList<>();
    }

    private final PayloadTypePredicate matchAllPredicate = new PayloadTypePredicate() {
        @Override
        public boolean match(TypedPayloadMetadata payload) {
            return true;
        }
    };

    @Override
    public ResponseInterface run(RequestInterface requestInterface, InstanceContainer instanceContainer) {
        Request request = (Request) requestInterface;
        Response response = new Response();

        Pair<IdentityDescriptor, String> identityResult = org.cweb.cli.Utils.tryLoadIdentity(instanceContainer, request.idStr);
        if (identityResult.getRight() != null) {
            response.error = identityResult.getRight();
            return response;
        }
        IdentityDescriptor identity = identityResult.getLeft();

        List<TypedPayload> messages = instanceContainer.getPrivateBroadcastService().readBroadcastsFrom(identity, matchAllPredicate);

        for (TypedPayload message : messages) {
            response.messages.add(TypedPayloadDecoder.decodePayload(message));
        }
        return response;
    }
}
