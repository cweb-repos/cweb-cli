package org.cweb.cli;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.communication.SharedSessionService;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.identity.IdentityProfile;
import org.cweb.schemas.identity.IdentityDescriptor;
import org.cweb.schemas.properties.Property;
import org.cweb.storage.NameConversionUtils;
import org.cweb.utils.PropertyUtils;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Utils {

    public static byte[] tryDecodingIdFromBase64or32(String idStr) {
        byte[] id = NameConversionUtils.fromString(idStr);
        if (!IdentityService.isValidId(id)) {
            id = org.cweb.utils.Utils.fromBase32String(idStr);
        }
        if (!IdentityService.isValidId(id)) {
            id = null;
        }
        return id;
    }

    public static Pair<IdentityDescriptor, String> tryLoadIdentity(InstanceContainer instanceContainer, String idStr) {
        byte[] id = tryDecodingIdFromBase64or32(idStr);
        if (id == null) {
            return Pair.of(null, "Invalid id");
        }
        IdentityDescriptor identity = instanceContainer.getRemoteIdentityService().get(id).getDescriptor();
        if (identity == null) {
            return Pair.of(null, "Failed to fetch identity descriptor");
        }
        return Pair.of(identity, null);
    }

    public static List<byte[]> getParticipants(InstanceContainer instanceContainer, String seedPeerIdStr, SharedSessionService.SessionMetadata metadata) {
        List<byte[]> participants;
        if (metadata != null) {
            participants = metadata.participantIds;
        } else {
            byte[] seedPeerId = Utils.tryDecodingIdFromBase64or32(seedPeerIdStr);
            if (seedPeerId == null || !instanceContainer.getCommSessionService().haveSessionWith(seedPeerId)) {
                errorOut("Missing or invalid seed peer");
            }
            participants = Collections.singletonList(seedPeerId);
        }
        return participants;
    }

    public static String getMessageShellPrefix(InstanceContainer instanceContainer, byte[] fromId, boolean isSelf, long time) {
        String from = null;
        IdentityProfile identityProfile = instanceContainer.getIdentityProfileReadService().getProfile(fromId);
        if (identityProfile != null) {
            Property nickName = PropertyUtils.findProperty(identityProfile.getProperties(), PropertyUtils.PROPERTY_KEY_NICK_NAME);
            if (nickName != null && nickName.getValue().isSetStr()) {
                from = nickName.getValue().getStr();
            }
        }
        if (from == null) {
            from = IdentityService.toString(fromId);
        }
        String now = org.cweb.utils.Utils.simpleDateFormat.format(new Date(System.currentTimeMillis()));
        String createdAt = org.cweb.utils.Utils.simpleDateFormat.format(new Date(time));
        return now + " / " + createdAt + " " + from + (isSelf ? " (me)" : "") + " > ";
    }

    static void errorOut(String error) {
        System.err.println(error);
        System.exit(1);
    }
}
