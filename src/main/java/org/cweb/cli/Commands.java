package org.cweb.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.cweb.cli.commands.CommSessionEstablish;
import org.cweb.cli.commands.CommSessionShell;
import org.cweb.cli.commands.Command;
import org.cweb.cli.commands.EndorsementProcessRequests;
import org.cweb.cli.commands.EndorsementProcessResponses;
import org.cweb.cli.commands.EndorsementSendRequest;
import org.cweb.cli.commands.FileDownload;
import org.cweb.cli.commands.FileDownloadsList;
import org.cweb.cli.commands.FileShare;
import org.cweb.cli.commands.FileUpload;
import org.cweb.cli.commands.FileUploadsDelete;
import org.cweb.cli.commands.FileUploadsList;
import org.cweb.cli.commands.IdentityCreateNew;
import org.cweb.cli.commands.IdentityPrintOwn;
import org.cweb.cli.commands.IdentityRead;
import org.cweb.cli.commands.IdentityReadStateless;
import org.cweb.cli.commands.IdentitySetOwnProperty;
import org.cweb.cli.commands.PrivateBroadcastRead;
import org.cweb.cli.commands.PrivateBroadcastSend;
import org.cweb.cli.commands.RequestInterface;
import org.cweb.cli.commands.CommSessionMessageRead;
import org.cweb.cli.commands.CommSessionMessageSend;
import org.cweb.cli.commands.SharedSessionCreate;
import org.cweb.cli.commands.SharedSessionList;
import org.cweb.cli.commands.SharedSessionRead;
import org.cweb.cli.commands.SharedSessionSend;
import org.cweb.cli.commands.SharedSessionShell;
import org.cweb.cli.commands.SharedSessionUpdateParticipants;
import org.cweb.cli.commands.StorageProfileSet;
import org.cweb.cli.commands.StorageProfileSetOwn;
import org.cweb.cli.commands.StorageProfileTest;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Commands {
    private static final List<? extends Command> commandsList = Lists.newArrayList(
            new IdentityCreateNew(),
            new IdentityPrintOwn(),
            new IdentityRead(),
            new IdentityReadStateless(),
            new IdentitySetOwnProperty(),
            new StorageProfileSetOwn(),
            new StorageProfileSet(),
            new StorageProfileTest(),
            new PrivateBroadcastSend(),
            new PrivateBroadcastRead(),
            new CommSessionEstablish(),
            new CommSessionMessageSend(),
            new CommSessionMessageRead(),
            new CommSessionShell(),
            new SharedSessionCreate(),
            new SharedSessionUpdateParticipants(),
            new SharedSessionList(),
            new SharedSessionSend(),
            new SharedSessionRead(),
            new SharedSessionShell(),
            new EndorsementSendRequest(),
            new EndorsementProcessRequests(),
            new EndorsementProcessResponses(),
            new FileShare(),
            new FileUpload(),
            new FileUploadsList(),
            new FileUploadsDelete(),
            new FileDownload(),
            new FileDownloadsList()
    );

    private final JCommander jc;
    private final Map<String, Command> name2commands = new LinkedHashMap<>();
    private final Map<String, RequestInterface> name2request = new LinkedHashMap<>();

    @Parameter(names = "-playground", description = "Simulate remote storage in local file system for playground mode")
    public boolean playground;

    @Parameter(names = "-localPath", description = "Local path for Cweb keys and cache")
    public String localPath;

    @Parameter(names = "-ownId", description = "Cweb ID on behalf of which to execute the command")
    public String ownIdStr;

    public Commands(String ... args) {
        jc = new JCommander(this);

        for (Command command : commandsList) {
            String name = decapitalize(command.getClass().getSimpleName());
            name2commands.put(name, command);
            RequestInterface request = command.getRequest();
            name2request.put(name, request);
            jc.addCommand(name, request);
        }

        jc.parse(args);
    }

    private static String decapitalize(String string) {
        if (string == null || string.length() == 0) {
            return string;
        }
        char c[] = string.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        return new String(c);
    }

    public String getCommandName() {
        return jc.getParsedCommand();
    }

    public Pair<Command, RequestInterface> getCommandAndRequest() {
        String command = jc.getParsedCommand();
        return Pair.of(name2commands.get(command), name2request.get(command));
    }

    public void usage() {
        jc.usage();
    }
}
