package org.cweb.cli;

import org.apache.commons.lang3.tuple.Pair;
import org.cweb.InstanceContainer;
import org.cweb.InstanceContainerStateless;
import org.cweb.cli.commands.AdminCommand;
import org.cweb.cli.commands.Command;
import org.cweb.cli.commands.RequestInterface;
import org.cweb.cli.commands.ResponseInterface;

public class CliMain {
    public static void main(String ... args) {
        CliMain main = new CliMain();
        Commands params = new Commands(args);
        main.run(params);
    }

    private void run(Commands commands) {
        Pair<Command, RequestInterface> commandAndRequest = commands.getCommandAndRequest();
        Command command = commandAndRequest.getLeft();
        RequestInterface request = commandAndRequest.getRight();
        if (command == null) {
            commands.usage();
            System.exit(1);
        }

        InstanceFactory instanceFactory = new InstanceFactory(commands);
        InstanceContainer instanceContainer = null;
        InstanceContainerStateless instanceContainerStateless = null;

        String responseStr;
        try {
            ResponseInterface response;
            if (command instanceof AdminCommand) {
                response = ((AdminCommand)command).run(request, instanceFactory);
            } else {
                if (command.isStateless()) {
                    instanceContainerStateless = instanceFactory.getReadOnlyInstanceContainer();
                    response = command.runStateless(request, instanceContainerStateless);
                } else {
                    instanceContainer = instanceFactory.getInstanceContainer();
                    response = command.run(request, instanceContainer);
                }
            }
            responseStr = instanceFactory.getMapper().writeValueAsString(response);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally{
            if (instanceContainer != null) {
                instanceContainer.flushUploadQueue(100);
                instanceContainer.close();
            }
            InstanceContainer.globalShutdown(1);
        }
        System.out.println(responseStr);
    }
}
