package org.cweb.cli;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.Sets;
import org.cweb.AccountUtils;
import org.cweb.Config;
import org.cweb.InstanceContainer;
import org.cweb.InstanceContainerBase;
import org.cweb.InstanceContainerStateless;
import org.cweb.communication.DefaultSchedulingProvider;
import org.cweb.identity.IdentityService;
import org.cweb.schemas.storage.PrivateStorageProfile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class InstanceFactory {
    private static final Set<PosixFilePermission> LOCAL_ROOT_PERMISSIONS = Sets.newHashSet(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE, PosixFilePermission.OWNER_EXECUTE);

    private final Commands commands;
    private final ObjectMapper mapper;

    public InstanceFactory(Commands commands) {
        this.commands = commands;
        this.mapper = new ObjectMapper();
        this.mapper.enable(SerializationFeature.INDENT_OUTPUT);
        this.mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        if (commands.playground) {
            Config.remoteStorageType = Config.RemoteStorageType.LOCAL_FILE_SYSTEM;
            Config.remoteStorageLocalFileSystemRootPath = getLocalRootPath();
        }

        if (commands.getCommandAndRequest().getLeft().isStateless()) {
            Config.localStorageType = Config.LocalStorageType.MEMORY;
        }
    }

    public ObjectMapper getMapper() {
        return mapper;
    }

    public InstanceContainer createNewIdentity(PrivateStorageProfile privateStorageProfile) {
        return AccountUtils.createNewIdentity(getLocalRootPath(), privateStorageProfile, null);
    }

    public InstanceContainerStateless getReadOnlyInstanceContainer() {
        return new InstanceContainerStateless();
    }

    public InstanceContainer getInstanceContainer() {
        String localRootPath = getLocalRootPath();
        InstanceContainer instanceContainer = new InstanceContainer(localRootPath, getLocalId(localRootPath), null);
        instanceContainer.getCommScheduler().init(new DefaultSchedulingProvider());
        return instanceContainer;
    }

    public InstanceContainer getInstanceContainer(byte[] id) {
        String localRootPath = getLocalRootPath();
        InstanceContainer instanceContainer = new InstanceContainer(localRootPath, id, null);
        instanceContainer.getCommScheduler().init(new DefaultSchedulingProvider());
        return instanceContainer;
    }

    private byte[] getLocalId(String localRootPath) {
        String idOrPrefix = commands.ownIdStr;

        List<byte[]> matchingIds = new ArrayList<>();
        File localRoot = new File(localRootPath);
        File[] files = localRoot.listFiles();
        if (localRoot.isDirectory() && files != null) {
            for (File file : files) {
                String fileName = file.getName();
                if (file.isDirectory() && (idOrPrefix == null || fileName.startsWith(idOrPrefix))) {
                    byte[] id = IdentityService.idFromString(fileName);
                    if (IdentityService.isValidId(id)) {
                        matchingIds.add(id);
                    }
                }
            }
        }

        if (matchingIds.isEmpty()) {
            Utils.errorOut("Matching id not found");
        }

        if (matchingIds.size() > 1) {
            Utils.errorOut("Multiple matching ids, please specify one via -ownId");
        }

        return matchingIds.get(0);
    }

    private String getLocalRootPath() {
        String path = commands.localPath;
        if (path == null) {
            String home = System.getProperty("user.home");
            path = home + File.separator + ".cweb";
        }
        if (commands.playground) {
            path += File.separator + "playground";
        }
        try {
            File pathFile = new File(path);
            if (pathFile.exists()) {
                if (!pathFile.isDirectory()) {
                    Utils.errorOut("Path " + path + " is not a directory");
                }
            } else {
                boolean success = pathFile.mkdirs();
                if (!success) {
                    Utils.errorOut("Failed to create local identity root at " + path);
                }
            }
            Set<PosixFilePermission> permissions = Files.getPosixFilePermissions(pathFile.toPath());
            if (!LOCAL_ROOT_PERMISSIONS.equals(permissions)) {
                Files.setPosixFilePermissions(pathFile.toPath(), LOCAL_ROOT_PERMISSIONS);
            }
        } catch (Exception e) {
            Utils.errorOut("Exception initializing local identity root at " + path + ": " + e.toString());
        }
        return path;
    }
}
