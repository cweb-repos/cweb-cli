#!/bin/sh

function usage() {
  echo "Usage: cweb-create-aws-s3-bucket.sh <AWS_REGION> <BUCKET_NAME> [PATH_PREFIX]"
}

if [ -z "$1" ] | [ -z "$2" ]; then
  echo "Missing arguments"
  usage
  exit 1
fi

region=$1
bucket=$2
pathPrefix=$3

if [ -z "$pathPrefix" ]; then
  pathPrefix="cweb"
fi

userNameReader="$bucket-$pathPrefix-reader"
userNameWriter="$bucket-$pathPrefix-writer"

main() {
  initTemplates
  userBucketAccess="$(instantiateTemplate $userBucketAccessTempl)"

  createBucket
  createReaderUser
  createWriterUser

  host="https://s3-$region.amazonaws.com"

  privateStorageProfile="s3!$host!$region!$bucket!$pathPrefix!$accessKeyIdRead!$secretAccessKeyRead!$accessKeyIdWrite!$secretAccessKeyWrite"
  echo "-------------- privateStorageProfile --------------"
  echo "$privateStorageProfile"
}

function createBucket() {
  createBucketResult=`aws s3api create-bucket --bucket "$bucket" --region "$region" --create-bucket-configuration LocationConstraint="$region" 2>&1`
  if [ $? -ne 0 ]; then
    if [[ $createBucketResult =~ "BucketAlreadyOwnedByYou" ]]; then
      echo "Bucket already exists $bucket"
      return
    fi
    echo "Error creating bucket: $createBucketResult"
    exit 1
  fi
  echo "Created bucket $bucket"
}

function createReaderUser() {
  userBucketReadAccess=${userBucketAccess//\{\{userBucketActions\}\}/"$(toOneLine $userBucketReadActions)"}
  createUser "$userNameReader" "$userBucketReadAccess"
  accessKeyIdRead=$accessKeyId
  secretAccessKeyRead=$secretAccessKey
}

function createWriterUser() {
  userBucketWriteAccess=${userBucketAccess//\{\{userBucketActions\}\}/"$(toOneLine $userBucketWriteActions)"}
  createUser "$userNameWriter" "$userBucketWriteAccess"
  accessKeyIdWrite=$accessKeyId
  secretAccessKeyWrite=$secretAccessKey
}

function createUser() {
  userName=$1
  bucketPolicy=$2

  createUserResult=`aws iam create-user --user-name "$userName"`
  if [ $? -ne 0 ]; then
    echo "Error creating user $userName"
    exit 1
  fi
  echo "Created user $userName"

  accessKeyResultRead=`aws iam create-access-key --user-name "$userName" --query 'AccessKey.[AccessKeyId,SecretAccessKey]' --output text`
  if [ $? -ne 0 ]; then
    echo "Error creating access key for $userName"
    exit 1
  fi
  IFS=$'\t' accessKeyResultReadArr=($accessKeyResultRead)
  accessKeyId="${accessKeyResultReadArr[0]}"
  secretAccessKey="${accessKeyResultReadArr[1]}"
  echo "Created access key for $userName"

  userPolicyResultRead=`aws iam put-user-policy --user-name "$userName" --policy-name cweb-bucket-read-access --policy-document "$bucketPolicy"`
  if [ $? -ne 0 ]; then
    echo "Error attaching policy to $userName"
    exit 1
  fi
  echo "Attached policy to $userName"
}

function instantiateTemplate() {
  instance=$@
  instance=${instance//\{\{bucketName\}\}/$bucket}
  instance=${instance//\{\{pathPrefix\}\}/$pathPrefix}
  echo "$instance"
}

function toOneLine() {
  echo "$@"
}

function initTemplates() {
  read -d '' userBucketAccessTempl <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        {{userBucketActions}}
      ],
      "Resource": [
        "arn:aws:s3:::{{bucketName}}/{{pathPrefix}}/*"
      ]
    }
  ]
}
EOF

  read -d '' userBucketReadActions <<EOF
        "s3:GetObject"
EOF

  read -d '' userBucketWriteActions <<EOF
        "s3:PutObject",
        "s3:PutObjectAcl",
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:DeleteObject",
        "s3:DeleteObjectVersion"
EOF
}

main "$@"
